# turniptgs 
telegram bot which can create whole pack from one animated sticker.

## requirements
python3.8
pip packages:
    aiogram,
    emoji

## setting up environment
1. create python3 virtual environment
    ```sh
    python -m venv .venv
    ```
2. activate virtual environment
    ```sh
    source .venv/bin/activate
    ```
3. install requirements
    ```sh
    pip install -r requirements.txt
    ```
4. set telegram token
    ```sh
    $EDITOR bot/config.py
    ```
5. run 
    ```sh
    python -m bot
    ```
