from aiogram.types import Message
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from .helpers import split

DATA_FIELDS = {"title": "Set title for your stickerpack",
               "name": "Set name for your stickerpack",
               "emojis": "Set emojis for your stickers",
               "sticker": "Send animated sticker"}


class Form(StatesGroup):
    title = State()
    name = State()
    emojis = State()
    sticker = State()


async def next_form(message: Message, state: FSMContext):
    async with state.proxy() as data:
        if any([not data.get(k) for k in DATA_FIELDS]):
            for k, v in DATA_FIELDS.items():
                if not data.get(k):
                    await message.answer(v)
                    await getattr(Form, k).set()
                    break
        else:
            del data["sticker"]
            a = await split(message, **data)
            if a:
                del data["title"]
                del data["emojis"]
                del data["name"]
            await state.finish()
