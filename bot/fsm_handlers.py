from aiogram.types import Message, ContentTypes
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from emoji import UNICODE_EMOJI

from . import dp
from .fsm import Form, next_form

VARIATION_SELECTORS = ['\fe00', '\fe01', '\fe02', '\fe03', '\fe04', '\fe05', '\fe06', '\fe07', '\fe08', '\fe09', '\fe0a', '\fe0b', '\fe0c', '\fe0d', '\fe0e', '\fe0f']
ZERO_WIDTH_JOINER = '\f200d'

@dp.message_handler(commands=['start'])
async def _(message: Message, state: FSMContext):
    await next_form(message, state)


@dp.message_handler(state='*', commands='cancel')
@dp.message_handler(Text(equals='cancel', ignore_case=True), state='*')
async def cancel_handler(message: Message, state: FSMContext):
    current_state = await state.get_state()
    if current_state is None:
        return

    await state.finish()
    await message.reply('Cancelled.')


@dp.message_handler(state=Form.title)
async def _(message: Message, state: FSMContext):
    async with state.proxy() as data:
        data["title"] = message.text

    await next_form(message, state)


@dp.message_handler(state=Form.name)
async def _(message: Message, state: FSMContext):
    async with state.proxy() as data:
        data["name"] = message.text

    await next_form(message, state)


@dp.message_handler(state=Form.emojis)
async def _(message: Message, state: FSMContext):
    if all(char in UNICODE_EMOJI or char in VARIATION_SELECTORS or char == ZERO_WIDTH_JOINER for char in message.text):
        async with state.proxy() as data:
            data["emojis"] = message.text

    await next_form(message, state)


@dp.message_handler(state=Form.sticker, content_types=ContentTypes.ANY)
async def _(message: Message, state: FSMContext):
    if not message.sticker:
        await message.reply("There's no sticker. Send me sticker.")
    elif not message.sticker.is_animated:
        await message.reply("This sticker is not animated."
                            " Send me animated sticker.")
    else:
        async with state.proxy() as data:
            data["sticker"] = message.sticker
        await next_form(message, state)
