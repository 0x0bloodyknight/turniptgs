import logging
import re
from io import BytesIO

from aiogram.types import Message, InputFile
from aiogram.types.sticker_set import Sticker, StickerSet
from aiogram.utils.exceptions import TelegramAPIError

from splitter import read_sticker, splitter, write_sticker
from . import bot, dp

flood_regex = re.compile(r"Flood control exceeded. Retry in (\d.+) seconds")


async def get_setname(name):
    bot_username = (await bot.me).username.lower()
    postfix = f"_by_{bot_username}"
    if not name.endswith(postfix):
        name += postfix
    return name


async def error_workaround(coro):
    try:
        return await coro
    except TelegramAPIError as e:
        desc = e.args[0]
        return desc


async def create_sticker_set(user_id, title, name,
                             emojis, tgs_sticker):
    name = await get_setname(name)
    sticker_set = await error_workaround(
        bot.create_new_sticker_set(user_id,
                                   name, title, emojis,
                                   tgs_sticker=tgs_sticker))
    return sticker_set


def bytes_sticker(sticker):
    s = BytesIO()
    write_sticker(sticker, s)
    s.seek(0)
    return s


async def add_sticker_to_set(user_id, name,
                             sticker, emojis, title,
                             notifier=None):
    name = await get_setname(name)

    s_set = await error_workaround(bot.get_sticker_set(name))

    desc = await error_workaround(
        bot.add_sticker_to_set(user_id, name, emojis,
                               tgs_sticker=bytes_sticker(sticker)))

    if isinstance(desc, str):
        if match := flood_regex.match(desc):
            s = int(match.group(1))

            if notifier:
                await notifier.answer(f"Flood control for {s} seconds.")

            await asyncio.sleep(s)

            return await add_sticker_to_set(user_id, name, sticker,
                                            emojis, title)
        elif desc == "Stickerset_invalid":
            ndesc = await create_sticker_set(user_id, title, name,
                                             emojis, bytes_sticker(sticker))
            return True
        return desc
    else:
        return True


async def split(message: Message, title, emojis, name):
    bot_username = (await bot.me).username.lower()
    user = message.from_user
    sticker = BytesIO()

    postfix = f"_by_{bot_username}"
    if not name.endswith(postfix):
        name += postfix

    await message.sticker.download(sticker)
    sticker = read_sticker(sticker)

    msg = await message.answer(f"Uploading stickers to t.me/addstickers/{name}\n"
                               f"This can take a while.")

    for s in splitter(sticker):
        success = await add_sticker_to_set(user.id, name, s, emojis, title)

        if isinstance(success, str):
            await message.reply(success)
            return

    await msg.delete()
    await message.answer(f"Done: t.me/addstickers/{name}")
    return True
